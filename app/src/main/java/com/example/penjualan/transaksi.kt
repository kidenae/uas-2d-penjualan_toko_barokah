package com.example.penjualan

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent

import android.view.View
import android.widget.Toast


import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.kelola_brg.*
import kotlinx.android.synthetic.main.kelola_brg.ed_harga
import kotlinx.android.synthetic.main.kelola_brg.ed_nama_brg
import kotlinx.android.synthetic.main.transaksi.*

import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class transaksi : AppCompatActivity(), View.OnClickListener {
    var url3 = "http://192.168.43.228/penjualan/query_up_del_ins.php"
    var url = "http://192.168.43.228/penjualan/show_data_tran.php"
    var daftarbrg = mutableListOf<HashMap<String,String>>()
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var tranAdapter: AdapterDataTran
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnscan -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btn_simpanTran ->{
                queryInsertUpdateDelete("insert_tran")
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult!=null){
            if(intentResult.contents != null){
                ed_nama_brg.setText(intentResult.contents)
                val strToken = StringTokenizer (ed_nama_brg.text.toString(),";",false)
                ed_nama_brg.setText(strToken.nextToken())
                ed_harga.setText(strToken.nextToken())
            } else {
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

        fun showDataBarang(){
            val request = StringRequest(
                Request.Method.POST,url,
                Response.Listener { response ->

                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        var brg = HashMap<String,String>()
                        brg.put("idQR",jsonObject.getString("idQR"))
                        brg.put("nama_brg",jsonObject.getString("nama_brg"))
                        brg.put("harga",jsonObject.getString("harga"))
                        daftarbrg.add(brg)
                    }
                    tranAdapter.notifyDataSetChanged()
                },
                Response.ErrorListener { error ->
                    Toast.makeText(this, "terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
                })
            val queue = Volley.newRequestQueue(this)
            queue.add(request)
        }
        fun queryInsertUpdateDelete( mode : String){
            val request = object : StringRequest(
                Method.POST,url3,
                Response.Listener { response ->
                    Log.i("info","["+response+"]")
                    val jsonObject = JSONObject(response)
                    val error = jsonObject.getString("kode")
                    if(error.equals("000")){
                        Toast.makeText(this,"Berhasil diupdate",Toast.LENGTH_LONG)
                            .show()
                        val intent = Intent(this,kelola_brg::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this,"Gagal diupdate",Toast.LENGTH_LONG)
                            .show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_LONG)
                        .show()
                }){
                override fun getParams(): MutableMap<String, String> {
                    val hm = HashMap<String,String>()

                    when(mode){
                        "insert_tran" -> {
                            hm.put("mode", "insert_tran")
                            hm.put("nama_brg",ed_nama_brg.text.toString())
                        }
                    }
                    return hm
                }
            }
            val queue = Volley.newRequestQueue(this)
            queue.add(request)
        }
    override fun onStart() {
        super.onStart()
        showDataBarang()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         intentIntegrator = IntentIntegrator(this)
        setContentView(R.layout.transaksi)
        btnscan.setOnClickListener(this)
        btn_simpanTran.setOnClickListener(this)
        tranAdapter = AdapterDataTran(daftarbrg)
        listTran.layoutManager = LinearLayoutManager(this)
        listTran.adapter = tranAdapter
    }
}