package com.example.penjualan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataTran(val datatran : List<HashMap<String,String>>) :
    RecyclerView.Adapter<AdapterDataTran.HolderDataBrg>(), View.OnClickListener {

    override fun onClick(v: View?) {

    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataTran.HolderDataBrg {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_tran,p0,false)
        return HolderDataBrg(v)
    }

    override fun getItemCount(): Int {
        return datatran.size
    }

    override fun onBindViewHolder(p0: AdapterDataTran.HolderDataBrg, p1: Int) {
        val data = datatran.get(p1)
        p0.txnama_brg.setText(data.get("nama_brg"))
        p0.txharga.setText("Rp. "+data.get("harga"))


    }

    class HolderDataBrg(v: View) : RecyclerView.ViewHolder(v){
        val txnama_brg = v.findViewById<TextView>(R.id.txnama_brg)
        val txharga = v.findViewById<TextView>(R.id.ed_harga2)


    }
}