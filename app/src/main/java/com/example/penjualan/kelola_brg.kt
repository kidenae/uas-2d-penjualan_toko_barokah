package com.example.penjualan

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent

import android.view.View
import android.widget.Toast


import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.kelola_brg.*
import kotlinx.android.synthetic.main.kelola_brg.ed_nama_brg

import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class kelola_brg : AppCompatActivity(), View.OnClickListener {
    val RC_IDBARANG_SUKSES : Int = 100
lateinit var brgAdapter: AdapterDataBrg
    var daftarbrg = mutableListOf<HashMap<String,String>>()
    var url = "http://192.168.43.228/penjualan/show_data.php"
    var url3 = "http://192.168.43.228/penjualan/query_up_del_ins.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTmbh_brg ->{
                val intent = Intent(this,input_brg::class.java)
                startActivity(intent)
            }
            R.id.btnEdit->{ queryInsertUpdateDelete("update")
            }
            R.id.btnDel->{ queryInsertUpdateDelete("delete")
            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kelola_brg)
        brgAdapter = AdapterDataBrg(daftarbrg)
        btnTmbh_brg.setOnClickListener(this)
        btnEdit.setOnClickListener(this)
        btnDel.setOnClickListener(this)
        listDataBrg.layoutManager = LinearLayoutManager(this)
         listDataBrg.adapter = brgAdapter
        listDataBrg.addOnItemTouchListener(itemTouch)
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }
    val itemTouch =  object :RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view  = p0.findChildViewUnder(p1.x, p1.y)
            val tag = p0.getChildAdapterPosition(view!!)
          load_id_Brg.setText(daftarbrg.get(tag).get("idQR"))
            ed_nama_brg.setText(daftarbrg.get(tag).get("nama_brg"))
            ed_harga.setText(daftarbrg.get(tag).get("harga"))
            ed_harga_jual.setText(daftarbrg.get(tag).get("harga_jual"))

            return false
        }
        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
    }
    fun showDataBarang(){
        val request = StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brg = HashMap<String,String>()
                    brg.put("idQR",jsonObject.getString("idQR"))
                    brg.put("nama_brg",jsonObject.getString("nama_brg"))
                    brg.put("harga",jsonObject.getString("harga"))
                    brg.put("harga_jual",jsonObject.getString("harga_jual"))
                    brg.put("url",jsonObject.getString("url"))
                    daftarbrg.add(brg)
                }
                brgAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun queryInsertUpdateDelete( mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil diupdate",Toast.LENGTH_LONG)
                        .show()
                    val intent = Intent(this,kelola_brg::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this,"Gagal diupdate",Toast.LENGTH_LONG)
                        .show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_LONG)
                    .show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()

                when(mode){
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("idQR",load_id_Brg.text.toString())
                        hm.put("nama_brg",ed_nama_brg.text.toString())
                        hm.put("harga",ed_harga.text.toString())
                        hm.put("harga_jual",ed_harga_jual.text.toString())

                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("idQR",load_id_Brg.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}