package com.example.penjualan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgKelola ->{
                val intent = Intent(this,kelola_brg::class.java)
                startActivity(intent)

            }
                R.id.imgTran ->{
                val intent = Intent(this,transaksi::class.java)
                startActivity(intent)

            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imgKelola.setOnClickListener(this)
        imgTran.setOnClickListener(this)
    }
}
