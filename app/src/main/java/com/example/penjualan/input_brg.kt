package com.example.penjualan

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.input_brg.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class input_brg : AppCompatActivity(), View.OnClickListener {
    lateinit var brgAdapter: AdapterDataBrg
    var url3 = "http://192.168.43.228/penjualan/query_up_del_ins.php"
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery() )
            }
            R.id.btnSimpan ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnGenerQR ->{

                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(ed_nama_brg.text.toString()+";"+ed_harga.text.toString(),
                    BarcodeFormat.QR_CODE,180,180)

                imQR.setImageBitmap(bitmap)
                imQR.setVisibility(View.VISIBLE)

            }
        }
    }
    lateinit var intenIntegrator :IntentIntegrator
    var imStr = ""

    lateinit var mediaHelper: MediaHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.input_brg)
        mediaHelper = MediaHelper(this)
        imUpload.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)
        intenIntegrator = IntentIntegrator(this)
        btnGenerQR.setOnClickListener(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data!!,imUpload)


            }

        }
        val intenResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intenResult!=null){
            if(intenResult.contents != null){
                ed_nama_brg.setText(intenResult.contents)

            } else {
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_LONG)
                    .show()
            }
        }
    }
 fun queryInsertUpdateDelete( mode : String){
     val request = object : StringRequest(Method.POST,url3,
         Response.Listener { response ->
             Log.i("info","["+response+"]")
             val jsonObject = JSONObject(response)
             val error = jsonObject.getString("kode")
             if(error.equals("000")){
                 Toast.makeText(this,"Berhasil diupdate",Toast.LENGTH_LONG)
                     .show()
                 val intent = Intent(this,kelola_brg::class.java)
                 startActivity(intent)
             } else {
                 Toast.makeText(this,"Gagal diupdate",Toast.LENGTH_LONG)
                     .show()
             }

         },
         Response.ErrorListener { error ->
             Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_LONG)
                 .show()
         }){
         override fun getParams(): MutableMap<String, String> {
             val hm = HashMap<String,String>()
             val nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                 .format(Date())+".jpg"
             when(mode){
                 "insert" -> {
                     hm.put("mode", "insert")
                     hm.put("idQR",txQR.text.toString())
                     hm.put("nama_brg",ed_nama_brg.text.toString())
                     hm.put("harga",ed_harga.text.toString())
                     hm.put("harga_jual",ed_harga_jual.text.toString())
                     hm.put("image",imStr)
                     hm.put("file",nmFile)

                 }
             }
             return hm
         }
     }
     val queue = Volley.newRequestQueue(this)
     queue.add(request)
 }


}