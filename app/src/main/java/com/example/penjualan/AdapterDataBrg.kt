package com.example.penjualan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataBrg(val databrg : List<HashMap<String,String>>) :
RecyclerView.Adapter<AdapterDataBrg.HolderDataBrg>(), View.OnClickListener{
    override fun onClick(v: View?) {

    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataBrg.HolderDataBrg {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_brg,p0,false)
        return HolderDataBrg(v)
    }

    override fun getItemCount(): Int {
        return databrg.size
    }

    override fun onBindViewHolder(p0: AdapterDataBrg.HolderDataBrg, p1: Int) {
        val data = databrg.get(p1)
        p0.txidBar.setText(data.get("idQR"))
        p0.txnama_brg.setText(data.get("nama_brg"))
        p0.txharga.setText(data.get("harga"))
        p0.txharga_jual.setText(data.get("harga_jual"))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.foto);
    }

    class HolderDataBrg(v: View) : RecyclerView.ViewHolder(v){
        val txidBar = v.findViewById<TextView>(R.id.txidBar)
        val txnama_brg = v.findViewById<TextView>(R.id.txnama_brg)
        val txharga = v.findViewById<TextView>(R.id.ed_harga)
        val txharga_jual = v.findViewById<TextView>(R.id.ed_harga_jual)
        val foto = v.findViewById<ImageView>(R.id.fotoo)
    }
}